#include <stdio.h>
#include <stdlib.h>
#if !defined(__APPLE__) // si no esta definido APPPLE (osea que si no estas sobre un mac,creo que asi es :P) include a malloc.h
#include <malloc.h>
#endif 
#include <string.h>
#include "Lista_vendedores.h"

#define TRUE 1
#define FALSE 0
#define LECTURA_MAXIMO 250


struct Lista_v *crear_v(){
	struct Lista_v *L = (struct Lista_v *)malloc(sizeof(struct Lista_v));
	L->sgte = NULL;
	L->nombre = NULL;	
	L->apellido = NULL;
	L->rut=NULL;
	L->direccion=NULL;
	L->tel_fijo=NULL;
	L->tel_movil=NULL;
	L->mail=NULL;
	L->tipo_contrato=NULL; 
	L->id=NULL;	
	return L;
};

char *sacar_caracter(char caracter, char *cadena){
	size_t i=0, j=0, largo=strlen(cadena);
        /*+1 para incluir el '\0' al final de la cadena*/
        char *nueva=malloc(sizeof(char)*largo+1);
        char *nueva2=NULL;

        memset(nueva, '\0', largo+1);

        for(i=0;i<largo;i++)
        {
                if(cadena[i]!=caracter)
                {
                       nueva[j]=cadena[i];
                       j++;
                }
        }


        nueva2=malloc(sizeof(char)*strlen(nueva)+1);
        strcpy(nueva2, nueva);
        free(nueva);

        return nueva2;
}

// obtiene el ID de 4 digitos del rut de vendedor
char *conseguir_id(char *rut){
	char *temp=sacar_caracter('-', rut);
	char *id=malloc(sizeof(char)*4);
	int cont=5, largo=strlen(temp), i;
	
	memset(id, '\0', 4);

	if(largo > 4){
		for(i=largo; i>=(largo-4) ;i-=1){
			if((largo-i) >=1){
				id[largo-i-1]=temp[largo-cont];
				cont-=1;
			}
		}
		id[4]='\0';
		free(temp);
		return id;
	}
	else{
		return NULL;
	}
	
}

void agregarVendedor(char *nombre,char *apellido, char *rut, char *direccion, char *tel_fijo, char *tel_movil, char *mail, char *tipo_contrato, struct Lista_v *p){

	struct Lista_v *temp = (struct Lista_v*)malloc(sizeof(struct Lista_v));

	temp->sgte=p->sgte;

	temp->nombre = nombre;
	temp->apellido = apellido;
	temp->rut = rut;
	temp->direccion = direccion;
	temp->tel_fijo = tel_fijo;
	temp->tel_movil = tel_movil;
	temp->mail=mail;
	temp->tipo_contrato = tipo_contrato;
	temp->id = conseguir_id(rut);	
	p->sgte = temp;	
}


void eliminarVendedor(struct Lista_v *p){
	struct Lista_v *q;
	q = p->sgte;
	p->sgte = p->sgte->sgte;
	free(q);
}
// retorna 1 si el id ingresado coincide con los id de los vendedores
int despedirVendedor(char *id, struct Lista_v *p){
	struct Lista_v *temp=p;
	while(temp->sgte != NULL)
	{
		if(strcmp(id, temp->sgte->id) == 0)
		{
			eliminarVendedor(temp);
			return 1;
		}
		temp=temp->sgte;
	}
	return 0;
}


void mostrarVendedores(struct Lista_v *p){
	struct Lista_v *temp=p;
	int i=0;
	if(temp->sgte == NULL){
		printf("lista vacia");
	}
	else{
		while (temp->sgte != NULL){
			printf("[%i] nombre: %s\tapellido: %s\tID: %s\n",i, temp->sgte->nombre, temp->sgte->apellido, temp->sgte->id);
			temp = temp->sgte;
			i+=1;
		}
	}
}


// el puntero p es el header de la lista, guarda toda la lista en el archivo
int guardar_lista_vendedores_ALL(struct Lista_v *p){
	FILE *fichero=NULL;
	fichero=fopen("vendedores.txt","w");
	struct Lista_v *temp = p;

	if(!fichero){
		// error cuando no se puede abrir
		return -1;
	}

	while(temp->sgte != NULL){
		// cuando es el ultimo vendedore que agregar, no inserta final de linea al final	
		if(temp->sgte->sgte == NULL){
			fprintf(fichero, "%s %s\n",  temp->sgte->nombre,temp->sgte->apellido);
			fprintf(fichero, "%s\n%s\n",temp->sgte->rut,temp->sgte->direccion);
			fprintf(fichero, "%s\n%s\n", temp->sgte->tel_fijo,temp->sgte->tel_movil);
			fprintf(fichero, "%s\n%s" ,temp->sgte->mail,temp->sgte->tipo_contrato);		
		}
		else{
			fprintf(fichero, "%s %s\n",  temp->sgte->nombre,temp->sgte->apellido);
			fprintf(fichero, "%s\n%s\n",temp->sgte->rut,temp->sgte->direccion);
			fprintf(fichero, "%s\n%s\n", temp->sgte->tel_fijo,temp->sgte->tel_movil);
			fprintf(fichero, "%s\n%s\n\n" ,temp->sgte->mail,temp->sgte->tipo_contrato);			
		}
		temp=temp->sgte;
	}

	if(!fclose(fichero)){		
		return 1;
	}
	else{
		//error no se pudo guardar
		return -1;
	}
}
// una idea. guarda solamente un vendedor, agregandolo al final del archivo
int guardar_vendedor(struct Lista_v *p){
	FILE *fichero=NULL;
	fichero=fopen("vendedores.txt","a");
	struct Lista_v *temp = p;

	if(!fichero){
		// error
		return -1;
	}
	else{
		fseek(fichero,0, SEEK_END); // se mueve al ultimo caracter

		fprintf(fichero, "\n\n%s %s\n",  temp->sgte->nombre,temp->sgte->apellido);
		fprintf(fichero, "%s\n%s\n",temp->sgte->rut,temp->sgte->direccion);
		fprintf(fichero, "%s\n%s\n", temp->sgte->tel_fijo,temp->sgte->tel_movil);
		fprintf(fichero, "%s\n%s" ,temp->sgte->mail,temp->sgte->tipo_contrato);
	}
	if(!fclose(fichero)){		
		return 1;
	}
	else{
		//error
		return -1;
	}
	return 0;
}
// busca y compara el id ingresado con los id de los vendedores
int buscar_id(char *id,struct Lista_v *p){
	struct Lista_v *temp=p;
	while(temp->sgte != NULL){
		if(strcmp(temp->sgte->id, id) == 0){
			return TRUE;
		}
		temp=temp->sgte;
	}
	return FALSE;
}

// funcion para cargar los datos desde el archivo .txt
int cargar_lista_vendedores(struct Lista_v *lista,struct Lista_v *final){
	FILE *fichero;
	char *nombre=NULL;
	char *apellido=NULL;
	char *rut=NULL;
	char *direccion=NULL;
	char *tel_fijo=NULL;
	char *tel_movil=NULL;
	char *mail=NULL;
	char *tipo_contrato=NULL;
	char *linea=NULL;
	int tamanio=-1;

	fichero = fopen("vendedores.txt","r");

	if(fichero==NULL){
		return -1;
	}

	tamanio = tamanio_archivo(fichero);
	if( (tamanio != (-1)) && (tamanio !=0) ){
		while( !feof(fichero) ){
			nombre=malloc(sizeof(char)*20);
			apellido=malloc(sizeof(char)*20);
			rut=malloc(sizeof(char)*20);
			direccion=malloc(sizeof(char)*20);
			tel_fijo=malloc(sizeof(char)*20);
			tel_movil=malloc(sizeof(char)*20);
			mail=malloc(sizeof(char)*20);
			tipo_contrato=malloc(sizeof(char)*10);
			linea=malloc(sizeof(char)*15);

			fscanf(fichero, "%s %s", nombre, apellido);			
			fgets(linea,4,fichero);

			fgets(rut,20,fichero);
			fgets(direccion,20,fichero);
			fgets(tel_fijo,20,fichero);
			fgets(tel_movil,20,fichero);
			fgets(mail,20,fichero);
			fgets(tipo_contrato,20,fichero);
			fgets(linea,10,fichero);
			// quita el salto del final para no tener problemas al escribir estos datos mas adelante
			rut = sacar_caracter('\n',rut);
			direccion = sacar_caracter('\n',direccion);
			tel_fijo = sacar_caracter('\n',tel_fijo);
			tel_movil = sacar_caracter('\n',tel_movil);
			mail = sacar_caracter('\n',mail);
			tipo_contrato = sacar_caracter('\n',tipo_contrato);


			agregarVendedor(nombre,apellido,rut,direccion,tel_fijo,tel_movil, mail, tipo_contrato, lista );
			
			nombre=NULL;
			apellido=NULL;
			rut=NULL;
			direccion=NULL;
			tel_fijo=NULL;
			tel_movil=NULL;
			mail = NULL;
			tipo_contrato=NULL;
			linea=NULL;
		}
		fclose(fichero);
		return 1;
	}
	else{
		return -1;
	}	
	return 0;
}


int comparar_ruts(char *rut, struct Lista_v *lista_v){
	struct Lista_v *temp=lista_v;
	while(temp->sgte != NULL){
		if(strcmp(temp->sgte->rut, rut) == 0){
			return TRUE;
		}
		temp=temp->sgte;
	}
	return FALSE;

}
// retorna el tamanio del archivo ingresado
unsigned int tamanio_archivo(FILE *archivo){
         unsigned int tamano;
         fseek(archivo, 0, SEEK_END); 	// vamos al ultimo caracter del texto
         tamano=ftell(archivo);		// devuelve la posicion actual del puntero, el cual corresponde al tamanio del texto
         fseek(archivo, 0, SEEK_SET);	// devuelve el puntero al principio del texto
         return tamano;
}

char *leer_teclado()
{
        char *buffer=malloc(sizeof(char)*LECTURA_MAXIMO);
        char *buffer2;

        buffer=fgets(buffer, LECTURA_MAXIMO, stdin);

        if(buffer)
		{        
                buffer2=malloc(sizeof(char)*strlen(buffer));

                strcpy(buffer2, buffer);
                free(buffer);
                buffer2=sacar_caracter('\n',buffer2); 
                return buffer2;
        }else{
                return NULL;
        }
}




