#ifndef _H_LISTAVENTAS_
#define _H_LISTAVENTAS_
#include "lista_boleta.h"
#include "Lista_clientes.h"
#include "lista_prod.h"


struct Lista_ventas
{
	int numeroBoleta;
	int pesosAcumulados;
	int totalVenta;
	int contCompras;
	char *productos;	
	char *rutCliente;	
	char *idVendedor;
	struct Lista_ventas *sgte;
};

struct Lista_ventas *crear_ventas();

int cargar_lista_boletas(struct Lista_ventas *lista);

unsigned int tamanio_archivo(FILE *archivo);

void agregar_venta(int numeroBoleta,char *productos , char *idVendedor, char *rutCliente, int pesosAcumulados, int totalVenta, struct Lista_ventas *p);

void eliminar_venta( struct Lista_ventas *p);

int guardar_lista_boletas( struct Lista_ventas *p);

void mostrarVentasDetalladas(char *id, struct Lista_ventas *p);

void mostrarVentasTotales(char *id, struct Lista_ventas *p);

int contarBoletas(char *id, struct Lista_ventas *p);

int contar_ventas(struct Lista_ventas *p); 

void mostrarBoletaSeleccion(int boletaSeleccion, struct Lista_ventas *p);

void mostrarComprasClientes(char *rut, struct Lista_ventas *lista_v);

void hacerVenta(struct Lista_c *lista_c, struct Lista_prod *lista_prod, struct Lista_ventas *lista_v, char *idVendedor);

struct Lista_ventas *ultima_venta(struct Lista_ventas *p);

struct Lista_ventas *buscar_venta(int numero, struct Lista_ventas *p);

int comprobar_rut(char *rut);

int char_to_int(char c);

#endif
