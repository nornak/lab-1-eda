#ifndef _H_LISTABOLETA_
#define _H_LISTABOLETA_

struct boletaD
{
	char *id;
	int cantProd;
	char *nombreProd;
	int precio;
	struct boletaD *sgte;
};

struct boletaD *crear();

void agregarBoletaD(char *id, char *nombreProd, int cantProd, int precio, struct boletaD *boleta);

void eliminarBoletaD(struct boletaD *p);

void eliminarLista(struct boletaD *h);

void mostrar_boletas(struct boletaD *h);

size_t largo_boleta(struct boletaD *lista_boleta);

char *listaBoleta_a_char(struct boletaD *lista_boleta);
#endif
