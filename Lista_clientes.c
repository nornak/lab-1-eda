#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#if !defined(__APPLE__) // si no esta definido APPPLE (osea que si no estas sobre un mac,creo que asi es :P) include a malloc.h
#include <malloc.h>
#endif 
#include "Lista_clientes.h"
#include "Lista_vendedores.h"

#define TRUE 1
#define FALSE 0

struct Lista_c *crear_c()
{
    struct Lista_c *L=(struct Lista_c *)malloc(sizeof(struct Lista_c));
    L->sgte=NULL;
    L->nombre = NULL;
    L->apellido = NULL;
    L->rut = NULL;
    L->calle = NULL;
    L->comuna=NULL;
    L->ciudad=NULL;
    L->tel_fijo=NULL;
    L->tel_movil=NULL;
    L->mail=NULL;
    L->descuento=0;
    L->cantCompras=0;
    return L;
};

void agregarCliente(char *nombre, char *apellido, char *rut, char *calle, char *comuna, char *ciudad, char *tel_fijo, char *tel_movil, char *mail,int descuento, int cantCompras, struct Lista_c *p)
{
    struct Lista_c *temp=(struct Lista_c*)malloc(sizeof(struct Lista_c));
    temp->sgte=p->sgte;
    temp->nombre=nombre;
    temp->apellido=apellido;
    temp->rut=rut;
    temp->calle=calle;
    temp->comuna=comuna;
    temp->ciudad=ciudad;
    temp->tel_fijo=tel_fijo;
    temp->tel_movil=tel_movil;
    temp->mail=mail;
    temp->descuento=descuento;
    temp->cantCompras=cantCompras;
    p->sgte=temp;
}

void listarClientes_rut(struct Lista_c *p)
{
    struct Lista_c *temp=p;
    if(temp->sgte==NULL)
    {
        printf("Lista Vacia\n");
    }
    else
    {
        while(temp->sgte != NULL)
        {
            printf("nombre: %s\t apellido: %s \trut: %s\n" ,temp->sgte->nombre, temp->sgte->apellido, temp->sgte->rut);
		    
            temp=temp->sgte;
        }
    }
}

void listarClientes_direccion(struct Lista_c *p)
{
    struct Lista_c *temp=p;
    if(temp->sgte==NULL)
    {
        printf("Lista Vacia\n");
    }
    else
    {
        while(temp->sgte != NULL)
        {
            printf("nombre: %s\t apellido: %s \tdireccion: %s %s %s\n" ,temp->sgte->nombre, temp->sgte->apellido, temp->sgte->calle, temp->sgte->comuna, temp->sgte->ciudad);
		    
            temp=temp->sgte;
        }
    }
}

void listarClientes_tel_fijo(struct Lista_c *p)
{
    struct Lista_c *temp=p;
    if(temp->sgte==NULL)
    {
        printf("Lista Vacia\n");
    }
    else
    {
        while(temp->sgte != NULL)
        {
            printf("nombre: %s\t apellido: %s \ttelefono fijo: %s\n" ,temp->sgte->nombre, temp->sgte->apellido, temp->sgte->tel_fijo);
		    
            temp=temp->sgte;
        }
    }
}

void listarClientes_tel_movil(struct Lista_c *p)
{
    struct Lista_c *temp=p;
    if(temp->sgte==NULL)
    {
        printf("Lista Vacia\n");
    }
    else
    {
        while(temp->sgte != NULL)
        {
            printf("nombre: %s\t apellido: %s \ttelefono movil: %s\n" ,temp->sgte->nombre, temp->sgte->apellido, temp->sgte->tel_movil);
		    
            temp=temp->sgte;
        }
    }
}

void listarClientes_mail(struct Lista_c *p)
{
    struct Lista_c *temp=p;
    if(temp->sgte==NULL)
    {
        printf("Lista Vacia\n");
    }
    else
    {
        while(temp->sgte != NULL)
        {
            printf("nombre: %s\t apellido: %s \tmail: %s\n" ,temp->sgte->nombre, temp->sgte->apellido, temp->sgte->mail);
		    
            temp=temp->sgte;
        }
    }
}

void listarClientes_descuento(struct Lista_c *p)
{
    struct Lista_c *temp=p;
    if(temp->sgte==NULL)
    {
        printf("Lista Vacia\n");
    }
    else
    {
        while(temp->sgte != NULL)
        {
            printf("nombre: %s\t apellido: %s descuento: %i\n" ,temp->sgte->nombre, temp->sgte->apellido, temp->sgte->descuento);
		    
            temp=temp->sgte;
        }
    }
}

void listarClientes_cantCompras(struct Lista_c *p)
{
    struct Lista_c *temp=p;
    if(temp->sgte==NULL)
    {
        printf("Lista Vacia\n");
    }
    else
    {
        while(temp->sgte != NULL)
        {
            printf("nombre: %s\t apellido: %s compras hechas: %i\n" ,temp->sgte->nombre, temp->sgte->apellido, temp->sgte->cantCompras);
		    
            temp=temp->sgte;
        }
    }
}

void modificarCliente(struct Lista_c *p)
{
    struct Lista_c *temp=p;
    int menu=TRUE;
    int opc;
    char *calle=NULL;
    char *comuna=NULL;
    char *ciudad=NULL;
    char *tel_fijo=NULL;
    char *tel_movil=NULL;
    char *mail=NULL;
    while(menu)
    {
        printf("Que dato desea modificar?\n");
        printf("1) Direccion\n");
        printf("2) Telefono fijo\n");
        printf("3) Telefono movil\n");
        printf("4) Mail\n");
	printf("5) salir\n");
        scanf("%i", &opc);
	fflush(stdin);
        switch(opc)
        {
            case 1: // modifica la direccion
		calle=malloc(sizeof(char)*15);
		comuna=malloc(sizeof(char)*15);
		ciudad=malloc(sizeof(char)*15);

		printf("direccion registrada: %s %s %s\n", temp->calle, temp->comuna, temp->ciudad);

                printf("Ingrese nueva calle:\n");
		calle=leer_teclado();
		printf("Ingrese nueva comuna:\n");
		comuna=leer_teclado();
		printf("Ingrese nueva ciudad:\n");
		ciudad=leer_teclado();
                temp->calle=calle;
		temp->comuna=comuna;
		temp->ciudad=ciudad;
		calle=NULL;
		comuna=NULL;
		ciudad=NULL;
                break;
            case 2: // modificar tel fijo
		tel_fijo=malloc(sizeof(char)*15);
		printf("telefono fijo registrado: %s\n", temp->tel_fijo);
                printf("Ingrese nuevo telefono fijo:\n");
		tel_fijo=leer_teclado();
                temp->tel_fijo=tel_fijo;
		tel_fijo=NULL;
                break;
            case 3: // modificar tel movil
		tel_movil=malloc(sizeof(char)*15);
		printf("telefono movil registrado: %s\n", temp->tel_movil);
                printf("Ingrese nuevo telefono movil:\n");
		tel_movil=leer_teclado();
                temp->tel_movil=tel_movil;
		tel_movil=NULL;
                break;
            case 4: // modificar mail
		mail=malloc(sizeof(char)*15);
		printf("mail registrado: %s\n", temp->mail);
                printf("Ingrese nuevo mail:\n");
		mail=leer_teclado();
                temp->mail=mail;
		mail=NULL;
                break;
	    case 5: // salir
		menu = FALSE;
		break;
            default:
                printf("Opcion no valida\n");
		break;
        }
    }
}

int guardar_lista_clientes(struct Lista_c *p){
	FILE *fichero=NULL;
	struct Lista_c *temp=p;

	fichero=fopen("clientes.txt","w");
	
	if(!fichero){
		return -1;
	}

	while(temp->sgte != NULL){
		if(temp->sgte->sgte == NULL){
			fprintf(fichero, "%s %s\n", temp->sgte->nombre, temp->sgte->apellido);
			fprintf(fichero, "%s\n%s\n", temp->sgte->rut, temp->sgte->calle);
			fprintf(fichero, "%s\n%s\n", temp->sgte->comuna, temp->sgte->ciudad);
			fprintf(fichero, "%s\n%s\n", temp->sgte->tel_fijo, temp->sgte->tel_movil);
			fprintf(fichero, "%s\n%i\n", temp->sgte->mail, temp->sgte->descuento);
			fprintf(fichero, "%i", temp->sgte->cantCompras);
		}
		else{
			fprintf(fichero, "%s %s\n", temp->sgte->nombre, temp->sgte->apellido);
			fprintf(fichero, "%s\n%s\n", temp->sgte->rut, temp->sgte->calle);
			fprintf(fichero, "%s\n%s\n", temp->sgte->comuna, temp->sgte->ciudad);
			fprintf(fichero, "%s\n%s\n", temp->sgte->tel_fijo, temp->sgte->tel_movil);
			fprintf(fichero, "%s\n%i\n", temp->sgte->mail, temp->sgte->descuento);
			fprintf(fichero, "%i\n\n", temp->sgte->cantCompras);
		}
		temp=temp->sgte;
	}

	if( !fclose(fichero))
		return 1;
	else
		return -1;
}

int guardar_cliente(struct Lista_c *p){
	FILE *fichero=NULL;
	struct Lista_c *temp=p;
	fichero=fopen("clientes.txt","a");

	if(!fichero)
		return -1;
	else{
		fseek(fichero, 0, SEEK_END);

		fprintf(fichero, "\n\n%s %s\n",  temp->sgte->nombre,temp->sgte->apellido);
		fprintf(fichero, "%s\n%s\n",temp->sgte->rut,temp->sgte->calle);
		fprintf(fichero, "%s\n%s\n", temp->sgte->comuna,temp->sgte->ciudad);
		fprintf(fichero, "%s\n%s\n" ,temp->sgte->tel_fijo, temp->sgte->tel_movil);
		fprintf(fichero, "%s\n%i\n", temp->sgte->mail,temp->sgte->descuento);
		fprintf(fichero, "%i", temp->sgte->cantCompras);
	}
	if(!fclose(fichero))
		return 1;
	else
		return -1;
}

int comparar_cliente_rut(char *rut, struct Lista_c *p){
	struct Lista_c *temp=p;

	while(temp->sgte != NULL){
		if( strcmp(temp->sgte->rut, rut)==0 ){
			return 1; 
		}
		temp=temp->sgte;
	}
	return 0;
}
struct Lista_c *buscar_cliente(char *nombre, char *apellido, struct Lista_c *p){
	struct Lista_c *temp = p;

	while(temp->sgte != NULL){
		if( (strcmp(temp->sgte->nombre, nombre)==0) && (strcmp(temp->sgte->apellido, apellido)==0) ){
			return temp->sgte;
		}
		temp=temp->sgte;
	}
	return NULL;
}

struct Lista_c *buscar_cliente_rut(char *rut, struct Lista_c *p){
	struct Lista_c *temp = p;

	while(temp->sgte != NULL){
		if( (strcmp(temp->sgte->rut, rut)==0) ){
			return temp->sgte;
		}
		temp=temp->sgte;
	}
	return NULL;
}

int cargarListaClientes(struct Lista_c *lista)
{
    FILE *archivo = NULL;    
    char *nombre =NULL;
    char *apellido =NULL;
    char *rut =NULL;
    char *calle =NULL;
    char *comuna =NULL;
    char *ciudad=NULL;
    char *tel_fijo =NULL;
    char *tel_movil =NULL;
    char *mail =NULL;
    char *linea =NULL;
    unsigned int descuento=0;
    int cantCompras=0;
    int  tamanio=-1;

    archivo = fopen("clientes.txt", "r");
    if(archivo == NULL){
	return -1;
    }
    tamanio = tamanio_archivo(archivo);

    if( (tamanio != (-1)) && (tamanio !=0) ){	
	    while(!feof(archivo))
	    {
	    	    nombre=malloc(sizeof(char)*15);
	    	    apellido=malloc(sizeof(char)*15);
	    	    rut=malloc(sizeof(char)*15);
	    	    calle=malloc(sizeof(char)*15);
		    comuna=malloc(sizeof(char)*15);
		    ciudad=malloc(sizeof(char)*15);		
	    	    tel_fijo=malloc(sizeof(char)*15);
	    	    tel_movil=malloc(sizeof(char)*15);
	    	    mail=malloc(sizeof(char)*15);
	    	    linea=malloc(sizeof(char)*4);

	    	    fscanf(archivo, "%s %s", nombre, apellido);
	    	    fgets(linea, 4, archivo);
	    	    fgets(rut, 15, archivo);
	    	    fgets(calle, 15, archivo);
		    fgets(comuna, 15, archivo);
		    fgets(ciudad, 15, archivo);
	    	    fgets(tel_fijo, 15, archivo);
	    	    fgets(tel_movil, 15, archivo);
	    	    fgets(mail, 15, archivo);
		    fscanf(archivo, "%i\n", &descuento);
		    fscanf(archivo, "%i\n", &cantCompras);
		    // quita el salto del final para no tener problemas al escribir estos datos mas adelante
		    rut=sacar_caracter('\n', rut);
		    calle=sacar_caracter('\n',calle);
		    comuna=sacar_caracter('\n', comuna);
		    ciudad=sacar_caracter('\n',ciudad);
		    tel_fijo=sacar_caracter('\n',tel_fijo);
		    tel_movil=sacar_caracter('\n',tel_movil);
		    mail=sacar_caracter('\n',mail);

		    agregarCliente(nombre, apellido, rut, calle, comuna, ciudad, tel_fijo, tel_movil, mail, descuento, cantCompras,  lista);

	    	    nombre=NULL;
	    	    apellido=NULL;
	    	    rut=NULL;
	    	    calle=NULL;
		    comuna=NULL;
		    ciudad=NULL;
	    	    tel_fijo=NULL;
	    	    tel_movil=NULL;
	    	    mail=NULL;
	    	    linea=NULL;
		    descuento = 0;
	    }
	    return 1;
    }
    else{
	    return -1;
    }
    fclose(archivo);
    return 0;
}
