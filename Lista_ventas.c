#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#if !defined(__APPLE__) // si no esta definido APPPLE (osea que si no estas sobre un mac,creo que asi es :P) include a malloc.h
#include <malloc.h>
#endif
#include "Lista_ventas.h"
#include "Lista_vendedores.h"
#include "lista_boleta.h"
#include "lista_clientes.h"
#include "lista_prod.h"

#define TRUE 1
#define FALSE 0



struct Lista_ventas *crear_ventas()
{
	struct Lista_ventas *l = (struct Lista_ventas *)malloc(sizeof(struct Lista_ventas));
	l->sgte=NULL;
	l->numeroBoleta=0;
	l->rutCliente=NULL;
	l->pesosAcumulados=0;
	l->productos=NULL;
	l->totalVenta=0;
	l->idVendedor=NULL;
	l->contCompras=0;
	return l;
};

void agregar_venta(int numeroBoleta,char *productos ,char *idVendedor, char *rutCliente, int pesosAcumulados, int totalVenta, struct Lista_ventas *p)
{
	
	struct Lista_ventas *temp = (struct Lista_ventas*)malloc(sizeof(struct Lista_ventas));
	temp->sgte=p->sgte;

	temp->numeroBoleta=numeroBoleta;
	temp->idVendedor=idVendedor;
	temp->rutCliente=rutCliente;
	temp->pesosAcumulados=pesosAcumulados;
	temp->totalVenta=totalVenta;
	temp->productos=productos;
	p->sgte=temp;
}

void eliminar_venta( struct Lista_ventas *p){
	struct Lista_ventas *q;
	q=p->sgte;
	p->sgte=p->sgte->sgte;
	free(q);	
}

int cargar_lista_boletas(struct Lista_ventas *lista_venta)
{
	FILE *fichero=NULL;
	int cantBoletas=0;
	int numeroBoleta=0;
	int totalVenta=0;
	int pesosAcumulados=0;
	int i, j, z, linea1=FALSE;
	int comprobador_numBoleta=FALSE, cont, cont2;
	int lim_izq, lim_der;
	int min=0;
	int cantidad=0;
	int largo_temp;
	char *lista=NULL;
	char *temp=NULL;
	char *aux=NULL;
	char *aux2=NULL;
	char *aux3=NULL;
	char *id=NULL;
	char *rutCliente=NULL, *productos=NULL, *idVendedor=NULL;
	unsigned int tamanio;
	struct Lista_ventas *lista_v_temp=NULL;
	struct boletaD *lista_b_temp=NULL;

	fichero=fopen("ventas.txt", "r");
	if (fichero == NULL){
		return -1;
	}
	// se obtiene el largo del texto
	tamanio=tamanio_archivo(fichero);
	lista=malloc(sizeof(char)*tamanio);
	// leemos todo el archivo y lo guardamos en lista
	fread(lista, sizeof(char), tamanio,fichero);

	lista_v_temp=crear_ventas();
	lista_b_temp=crear();
	id=malloc(sizeof(char)*5);
	memset(id, '\0', 6);
	
	for(i=0; i<tamanio; i++)
	{	
		//printf("%c[%i] ", lista[i],i);
		
		if((lista[i]=='\n') || (lista[i]=='\0') )
		{			
			temp=malloc(sizeof(char)*(i-min+1));
			aux=malloc(sizeof(char)*10);			
			memset(temp, '\0', i-min+2);
			memset(aux, '\0', 11);
			printf("i-min:%i\n",(i-min));
			// guarda toda la linea en temp
			for(j=min; j<i; j++)
			{
				temp[j-min]=lista[j];
			}

			temp[j-min]='\0';
			min=i+1;
			// guardamos la cantidad de boletas
			if((sscanf(temp, "%i", &cantBoletas)==1) && (linea1==FALSE))
			{
				linea1=TRUE;
			}
			// guardamos el numero de una boleta
			else if( sscanf(temp, "%i", &numeroBoleta)==1 && (comprobador_numBoleta==FALSE))
			{
				comprobador_numBoleta=TRUE;
			}
			// guardamos el id del vendedor
			else if( strlen(temp)==4 )
			{
				printf("\n3 if\n");
				idVendedor=temp;
			}
			// guadamos el rut del cliente
			else if( comprobar_rut(temp) == 1 )
			{
				printf("\n4 if\n");
				rutCliente=temp;
			}
			// guardamos el producto comprado
			else if( (isdigit(temp[0])==1) && (isdigit(temp[1])==1) /*&&  (sscanf(temp, "%s %s %s %s", aux,aux,aux,aux )==4*/ )
			{				
				printf("\n5 if\n");
			
				largo_temp=strlen(temp);
				cont=0;	
				// obtenemos los limites del nombre del producto
				// avanzamos desde el inicio haste encontrar 2 espacios vacios
				for(z=0; z<tamanio; z++)
				{
					if(temp[z]==' ')
						cont++;
						
					if(cont==2){
						lim_izq=z;
						break;
					}
				}
				cont=0;
				// avanzamos desde el final hasta encontrar un espacio vacio
				for(z=tamanio; z>=0; z--)
				{
					if(temp[z]==32)
						cont++;
					if(cont==1){
						lim_der=z;
						break;
					}
				}
				
				aux=malloc(sizeof(char)*(lim_izq));
				aux2=malloc(sizeof(char)*(lim_der-lim_izq));
				aux3=malloc(sizeof(char)*(tamanio-lim_der));
				memset(aux,'\0', lim_izq);
				memset(aux2,'\0', (lim_der-lim_izq));
				memset(aux3,'\0', (tamanio-lim_der));
				cont=0;
				cont2=0;
				printf("lenght: %i, lim_izq: %i, lim_der: %i\n",tamanio,lim_izq,lim_der);
				for(z=0; z<tamanio; z++)
				{
					if(z<lim_izq){
						printf("1_%c\n", temp[z]);
						aux[z]=temp[z];
					}
					else if(z>lim_izq && z<lim_der){
						printf("2_%c\n", temp[z]);
						aux2[cont]=temp[z];
						cont++;
					}
					else if(z>lim_der){
						printf("3_%i\n", temp[z]);
						aux3[cont2]=temp[z];
						cont2++;
					}
				}
				printf("%s-%s-%s-",aux,aux2,aux3);
				lista_v_temp->totalVenta=atoi(aux3);
				lista_v_temp->productos=aux2;
				sscanf(aux, "%s %i",id,&cantidad);
				agregarBoletaD(id, aux2, cantidad, atoi(aux3), lista_b_temp);
			
				
			}
			free(temp);
			temp=NULL;
		}
		
	}
	printf("\n");
	mostrar_boletas(lista_b_temp);
	mostrarVentasDetalladas("1922", lista_v_temp);
	return 0;
}


int guardar_lista_ventas( struct Lista_ventas *p)
{
	int cantBoletas=contar_ventas(p);
	FILE *fichero = NULL;
	fichero=fopen("ventas.txt", "w");
	struct Lista_ventas *temp = p;

	if (!fichero){
		return -1;
	}
	
	fprintf(fichero, "%i\n", cantBoletas);
	while(temp->sgte != NULL){
		if(temp->sgte->sgte == NULL){
			fprintf(fichero, "%i\n", temp->sgte->numeroBoleta);
			fprintf(fichero, "%s\n", temp->sgte->idVendedor);
			fprintf(fichero, "%s\n", temp->sgte->rutCliente);
			fprintf(fichero, "%s\n", temp->sgte->productos);
			fprintf(fichero, "%i\n", temp->sgte->pesosAcumulados);
			fprintf(fichero, "%i\n", temp->sgte->totalVenta);
		}
		else{
			fprintf(fichero, "%i\n", temp->sgte->numeroBoleta);
			fprintf(fichero, "%s\n", temp->sgte->idVendedor);
			fprintf(fichero, "%s\n", temp->sgte->rutCliente);
			fprintf(fichero, "%s\n", temp->sgte->productos);
			fprintf(fichero, "%i\n", temp->sgte->pesosAcumulados);
			fprintf(fichero, "%i\n\n", temp->sgte->totalVenta);
		}
		temp=temp->sgte;
	}
	if(!fclose(fichero)){
		return 1;
	}	
	else
		return -1;
}
// funcion admin, muestra las ventas detalladas realizadas por un vendedor 
void mostrarVentasDetalladas(char *id, struct Lista_ventas *p)
{
	struct Lista_ventas *temp=p;
	while(temp->sgte != NULL){
		printf("id: %s\n", temp->sgte->idVendedor);
		if(strcmp(id, temp->sgte->idVendedor)==0){
			printf("Numero boleta: %i\n", temp->sgte->numeroBoleta);
			printf("Rut Cliente: %s\n", temp->sgte->rutCliente);
			printf("Pesos acumulados: %i\n", temp->sgte->pesosAcumulados);
			printf("%s", temp->sgte->productos);
			printf("TotalVenta: %i\n\n", temp->sgte->totalVenta);
		}
		temp=temp->sgte;	
	}
}
// muestra ventas totales de un vendedor
void mostrarVentasTotales(char *id, struct Lista_ventas *p)
{
	struct Lista_ventas *temp=p;
	int ventaTotal = 0;
	while(temp->sgte != NULL){
		if(strcmp(id, temp->sgte->idVendedor)==0){
			ventaTotal=ventaTotal+temp->sgte->totalVenta; 
		}
		temp=temp->sgte;
	}
	printf("\nLa venta total del vendedor es: %i\n", ventaTotal);
}

int contarBoletas(char *id, struct Lista_ventas *p){
	struct Lista_ventas *temp=p;
	int contador=0;
	while(temp->sgte != NULL){
		if(strcmp(id, temp->sgte->idVendedor)==0){
			contador=contador+1;
		}
		temp=temp->sgte;
	}
	return contador;
}
// muestra una boleta en particular independiente del vendedor
void mostrarBoletaSeleccion(int boletaSeleccion, struct Lista_ventas *p)
{
	struct Lista_ventas *temp=p;
	while(temp->sgte != NULL){
		if(boletaSeleccion == temp->sgte->numeroBoleta){
			printf("Numero boleta: %i\n", temp->sgte->numeroBoleta);
			printf("Rut Cliente: %s\n",temp->sgte->rutCliente);
			printf("%s", temp->sgte->productos);
			printf("Pesos acumulados: %i\n", temp->sgte->pesosAcumulados);
			printf("TotalVenta: %i\n\n", temp->sgte->totalVenta);
		}
		temp=temp->sgte;
	}
}

void mostrarComprasClientes(char *rut, struct Lista_ventas *lista_v)
{
	struct Lista_ventas *temp=lista_v;
	while(temp->sgte != NULL){
		if(strcmp(rut, temp->sgte->rutCliente)==0){
			printf("Numero boleta: %i\n", temp->sgte->numeroBoleta);
			printf("Rut Cliente: %s\n",temp->sgte->rutCliente);
			printf("%s", temp->sgte->productos);
			printf("Pesos acumulados: %i\n", temp->sgte->pesosAcumulados);
			printf("TotalVenta: %i\n\n", temp->sgte->totalVenta);
		}
		temp=temp->sgte;
	}

}
void hacerVenta(struct Lista_c *lista_c, struct Lista_prod *lista_prod, struct Lista_ventas *lista_v, char *idVendedor)
{	
	struct Lista_c *temp=NULL;
	struct Lista_prod *temp2=NULL;
	struct boletaD *lista_boleta=NULL;
	char *rut=NULL;
	char *id=NULL;
	char *opcion=NULL;
	char *productos=NULL;
	int pregunta=TRUE;
	int menu=TRUE;
	int cantComprada=0;	
	int totalVenta=0; // precio de la suma de todos los productos
	int precioTotal=0; // precio total de cada producto, = cantidad*precioProducto
	int pesosAcumulados=0;	
	int cantidad_ventas=contar_ventas(lista_v);
		
	rut=malloc(sizeof(char)*15);
	memset(rut, '\0', 15);

	printf("\n\tVenta\n\n");
	printf("Ingrese rut del cliente:\n");
	rut=leer_teclado();
	if(comprobar_rut(rut)==1){
		temp=buscar_cliente_rut(rut, lista_c);
		
		printf("\nCliente:\nNombre: %s apellido: %s\n\n", temp->nombre,temp->apellido);
		if(temp != NULL)
		{
			lista_boleta=crear();
			printf("Ingrese los datos de los productos comprados\n");
			while(menu==TRUE)
			{
				id=malloc(sizeof(char)*5);			
	
				printf("ID del producto(q salir):\n");
				id=leer_teclado();
			
				if(strcmp(id, "q")==0)
				{
					menu=FALSE;
				}
				else{
					temp2=buscar_id_prod(id, lista_prod);
					if(temp2 != NULL)
					{
						int aux;
						aux=temp2->precio;
						printf("cantidad:\n");
						scanf("%i", &cantComprada);
						fflush(stdin);
						while(cantComprada <0)
						{
							printf("valor ingresado invalido\n");
							printf("ingrese nueva cantidad:\n");
							scanf("%i", &cantComprada);
							fflush(stdin);
						}					
						
						if(restar_stock(cantComprada, temp2) == 1 )
						{
							precioTotal=cantComprada*(aux);
							totalVenta+=precioTotal;
							agregarBoletaD(id, temp2->producto ,cantComprada, precioTotal, lista_boleta);
							printf("id: %s, producto: %s, cantidad: %i, precio: %i\n", id, temp2->producto, cantComprada, precioTotal);
						}
						else{
							printf("no hay existencias\n");
						}		
					}
					else{
						printf("id del producto no encontrado: %s\n", id);
					}
				}			
			}
			printf("\ttolal a pagar: %i\n\n", totalVenta);
	
			productos=listaBoleta_a_char(lista_boleta);
			pesosAcumulados= totalVenta*10/100;
			temp->descuento+=pesosAcumulados;
			temp->cantCompras+=1;
			if(temp->cantCompras%5 == 0){
				printf("Esta disponible la opcion de aplicar el descuento\nTiene un descuento de: $%i\nAplicar descuento?(si-no):", temp->descuento);
				opcion=leer_teclado();
				while(pregunta){
					if( (strcmp(opcion, "si")==0) || (strcmp(opcion, "Si")==0) || (strcmp(opcion, "s")==0)){
						printf("descuento aplicado\n");
						totalVenta-=temp->descuento;
						if(totalVenta < 0){
							temp->descuento= totalVenta*(-1);
							totalVenta=0;
						}else{
							temp->descuento=0;
							temp->cantCompras=0;
							pesosAcumulados=0;
						}
						printf("\n\tnuevo total: %i\n", totalVenta);
						printf("\tnuevo descuento: %i\n\n", temp->descuento);
						pregunta=FALSE;
					}else if( (strcmp(opcion, "no")==0) || (strcmp(opcion, "No")==0) || (strcmp(opcion, "n")==0)){
						pregunta=FALSE;
					}else{
						printf("opcion invalida.\n");
						printf("ingrese nuevamente\n");
						opcion=leer_teclado();
					}
				}	
			}

			// el n� de boleta es uno mas a la cantidad de ventas
			agregar_venta((cantidad_ventas+1), productos, idVendedor, rut,pesosAcumulados, totalVenta, ultima_venta(lista_v));
			guardar_lista_ventas(lista_v);
			guardar_lista_clientes(lista_c);
	
			eliminarLista(lista_boleta);
			totalVenta=0;
			productos=NULL;
			temp=NULL;
			temp2=NULL;
			free(rut);
			free(id);
			rut=NULL;
		}else{
			printf("Ningun cliente registrado coincide con el rut ingresado\n");	
		}
	}
	else{
		printf("el rut ingresado es invalido\n");	
	}
}
// devuelve la cantidad de ventas de la lista
int contar_ventas(struct Lista_ventas *p){
	struct Lista_ventas *temp=p;
	int contador=0;
	while(temp->sgte != NULL){
		contador=contador+1;
		temp=temp->sgte;
	}
	return contador;
}

struct Lista_ventas *ultima_venta(struct Lista_ventas *p){
	struct Lista_ventas *temp=p;
	while(temp->sgte != NULL){
		temp=temp->sgte;
	}
	
	return temp;
}

struct Lista_ventas *buscar_venta(int numero, struct Lista_ventas *p){
	struct Lista_ventas *temp=p;
	while(temp->sgte != NULL){
		if(numero == temp->sgte->numeroBoleta){
			return temp;
		}
		temp=temp->sgte;
	}
	
	return NULL;
}

int comprobar_rut(char *rut){
	char *rut2=NULL;
	int largo, cont=2, i=0,suma=0, resto, diferencia;

	rut2=malloc(sizeof(char)*strlen(rut));
	rut2=sacar_caracter('-', rut);	
	largo=strlen(rut2);

	for(i=largo-2;i>=0;i--){
		suma+= char_to_int(rut2[i])*cont;
		cont++;
		if(cont==8){
			cont=2;
		}
	}
	resto=suma%11;
	diferencia=11-resto;
	// sumo 48 para pasarlo a ACSII y asi comparar el ultimo caracter del rut
	if( (diferencia+48) == rut2[largo-1] ){
		return 1;
	}
	else
		return 0;
}
// compara el caracter con los valores de los numero en ACSII
int char_to_int(char c){
	switch(c){
		case 48:
			return 0;
		case 49:
			return 1;
		case 50:
			return 2;
		case 51:
			return 3;
		case 52:
			return 4;
		case 53:
			return 5;
		case 54:
			return 6;
		case 55:
			return 7;
		case 56:
			return 8;
		case 57:
			return 9;
		default:
			return -1;
	}
}
