#include "lista_prod.h"
#include <stdlib.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include "lista_vendedores.h"


struct Lista_prod *crear_prod()
{
	struct Lista_prod *l=(struct Lista_prod *)malloc(sizeof(struct Lista_prod));
	l->sgte=NULL;
	l->id=NULL;
	
	return l;
};


int cargar_prod(struct Lista_prod *p)
{
	FILE *fichero=NULL;
	char *id=NULL;
	char *producto=NULL;
	char *linea=NULL;
	int precio=0;
	int stock=0;
	int tamanio=-1;

	fichero=fopen("productos.txt", "r");
	if (fichero == NULL){
		return -1;
	}
	tamanio=tamanio_archivo(fichero);
	if((tamanio != (-1)) && (tamanio != 0))
	{
		while( !feof(fichero))
		{
			id=malloc(sizeof(char)*5);
			producto=malloc(sizeof(char)*15);
			
			fgets(id, 5, fichero);			
			fgets(producto, 15, fichero);
			fscanf(fichero, "%i\n", &precio);
			fscanf(fichero, "%i\n", &stock);
		
			id=sacar_caracter('\n',id);
			producto=sacar_caracter('\n', producto);

			agregar_prod(id, producto, precio, stock, p);	

			id=NULL;
			producto=NULL;		
			linea=NULL;	

		}
		fclose(fichero);
		return 1;
	}
	else{
		return -1;
	}
	return 0;
}

void agregar_prod(char *id, char *producto, int precio, int stock, struct Lista_prod *lista_p)
{
	struct Lista_prod *temp = (struct Lista_prod *)malloc(sizeof(struct Lista_prod));

	temp->sgte=lista_p->sgte;	
	temp->id=id;
	temp->producto=producto;
	temp->precio=precio;
	temp->stock=stock;	
	lista_p->sgte = temp;		
}

void mostrar_productos(struct Lista_prod *p)
{
	struct Lista_prod *temp=p;

	if(temp->sgte!=NULL){
		while(temp->sgte != NULL){
			printf("id: %s,\tnombre: %s, \tprecio: %i, \tstock: %i\n",temp->sgte->id, temp->sgte->producto, temp->sgte->precio,temp->sgte->stock);
			temp=temp->sgte;
		}	
	}
}
// retorna un puntero al nodo que tiene el id ingresado
struct Lista_prod *buscar_id_prod(char* id, struct Lista_prod *p)
{
	struct Lista_prod *temp=p;

	while(temp->sgte != NULL){
		if(strcmp(temp->sgte->id, id)==0){
			return temp->sgte;
		}
		temp=temp->sgte;
	}
	return NULL;
}

int restar_stock(int cant, struct Lista_prod *lista){
	if((lista->stock - cant) >= 0 ){
		lista->stock-=cant;
		return 1;
	}
	return 0;
}


int prod_obtener_precio(struct Lista_prod *p){
	if(p!=NULL){
		return p->precio;	
	}
	return 0;
}







