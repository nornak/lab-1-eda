#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if !defined(__APPLE__) // si no esta definido APPPLE (osea que si no estas sobre un mac,creo que asi es :P) include a malloc.h
#include <malloc.h>
#endif  // eso es requerido cuando usas if
#include <ctype.h>
#include "Lista_vendedores.h"
#include "Lista_clientes.h"
#include "Lista_ventas.h"
#include "lista_boleta.h"
#include "lista_prod.h"

#define TRUE 1
#define FALSE 0

//variable global
struct Lista_v *lista_v;
struct Lista_c *lista_c;
struct Lista_v *final;
struct Lista_ventas *lista_ventas;
struct boletaD *boleta_d;
struct Lista_prod *lista_prod;

void menuAdmin()
{
    int opc;
    int menu = TRUE;
    int aux=0;
    char *nombre = NULL;
    char *apellido = NULL;
    char *rut = NULL;
    char *direccion = NULL;
    char *tel_fijo = NULL;
    char *tel_movil = NULL;
    char *mail = NULL;
    char *tipo_contrato = NULL;
    char *id = NULL;
    int boletaSeleccion = 0;
    printf("\n\tMenu del Administrador.\n\n");
	
    while(menu){
	    opc=0;
    	    printf("\nSeleccione que desea hacer:\n\n");
	    printf("1) Contratar nuevo vendedor\n");
	    printf("2) Eliminar vendedor\n");
	    printf("3) Listar vendedores\n");
	    printf("4) Mostrar ventas totales de un vendedor\n");
	    printf("5) Mostrar ventas detalladas de un vendedor\n");
	    printf("6) Mostrar una boleta en particular\n");
	    printf("7) Modificar datos de un cliente\n");
	    printf("8) Salir del menu de administrador\n");
	    scanf("%i", &opc);
	    fflush(stdin);
	    switch(opc)
	    {            
		    case 1: // contratar nuevo vendedor
			    nombre=malloc(sizeof(char)*15);
			    apellido=malloc(sizeof(char)*15);
			    rut=malloc(sizeof(char)*19);
			    direccion=malloc(sizeof(char)*14);
			    tel_fijo=malloc(sizeof(char)*15);
			    tel_movil=malloc(sizeof(char)*15);
			    mail=malloc(sizeof(char)*15);
			    tipo_contrato=malloc(sizeof(char)*15);
			    id = malloc(sizeof(char)*10);

			    printf("\nIngrese los datos: \n");
			    printf("Nombre:");
			    nombre=leer_teclado();
			    printf("apellido:");
			    apellido=leer_teclado();
			    printf("rut:");
			    rut=leer_teclado();
			    printf("direccion:");
			    direccion=leer_teclado();
			    printf("telefono fijo:");
			    tel_fijo=leer_teclado();
			    printf("telefono movil:");
			    tel_movil=leer_teclado();
			    printf("mail:");
			    mail=leer_teclado();
			    printf("tipo contrato:");
			    tipo_contrato=leer_teclado();

			    if(comprobar_rut(rut) == TRUE){	
				    // compara si ya exite el rut ingresado con algunos de los vendedores de la lista				    
				    if( comparar_ruts(rut, lista_v) != TRUE )
				    {
					    agregarVendedor(nombre, apellido, rut, direccion, tel_fijo,tel_movil, mail, tipo_contrato, lista_v);
    					    if( guardar_vendedor(lista_v) == (-1) )
						    printf("[!] Error al actualizar el archivo\n");								    
    					    else
						    printf("Actualizada con exito! \n");
				    }else
					    printf("ya existe un vendedor con el rut ingresado\n.El vendedor no fue cantratado\n");
			    }
			    else
				    printf("\nRut invalido\n");
			    
			    nombre=NULL;
			    apellido = NULL;
			    rut = NULL;
			    direccion = NULL;
			    tel_fijo = NULL;
			    tel_movil = NULL;
			    mail = NULL;
			    tipo_contrato = NULL;
			    break;

		    case 2: // eliminar vendedor
			    id = malloc(sizeof(char)*10);
			    printf("Ingrese ID del vendedor a eliminar:\n");
			    id=leer_teclado();
			    if(despedirVendedor(id, lista_v) == 1){
				    printf("vendedor eliminado con exito\n");

				    if(guardar_lista_vendedores_ALL(lista_v) == 1)
					    printf("guardado y cerrado\n");
				    else
					    printf("error al guardar\n");
			    }
			    else{
				    printf("ID no encontrado\n");
			    }
			    id = NULL;
			    break;
		    case 3: // mostrar vendedores
			    mostrarVendedores(lista_v);
			    break;
		    case 4: // mostrar ventas totales de un vendedor
			    id = malloc(sizeof(char)*10);
			    printf("Ingrese ID del vendedor para mostrar su venta total:\n");
			    id=leer_teclado();
			    mostrarVentasTotales(id, lista_ventas);
			    id=NULL;
			    break;						
		    case 5: // mostar ventas detalladas de un vendedor
			    id = malloc(sizeof(char)*10);
			    printf("Ingrese ID del vendedor para mostrar sus ventas detalladas:\n");
			    id=leer_teclado();
			    mostrarVentasDetalladas(id,lista_ventas);
			    id=NULL;
			    break;
		    case 6: // mostrar una boleta en particular
			    id = malloc(sizeof(char)*10);
			    printf("Ingrese ID del vendedor a buscar boleta:\n");
			    id=leer_teclado();
			    aux= contarBoletas(id, lista_ventas);
			    if(aux!=0){
				    printf("El Vendedor dispone de: %i boletas\n", aux);
				    printf("Que boleta desea ver?\n");
				    scanf("%i", &boletaSeleccion);
				    fflush(stdin);
				    mostrarBoletaSeleccion(boletaSeleccion, lista_ventas);
			    }
			    else{
				    printf("El vendedor no tiene boletas");
			    }
			    id=NULL;
			    break;
		    case 7: // modificar datos de un cliente
			    rut=nombre=malloc(sizeof(char)*15);				
			    struct Lista_c *temp= NULL;
			    char *opcion=malloc(sizeof(char)*5);
			    int pregunta=TRUE;
			    
			    printf("desea ver la lista de los clientes?(si-no)\n");
			    opcion=leer_teclado();
			    while(pregunta){
				    if( (strcmp(opcion, "si")==0) || (strcmp(opcion, "Si")==0) || (strcmp(opcion, "s")==0)){
					    listarClientes_rut(lista_c);
					    pregunta=FALSE;
				    }
				    else if( (strcmp(opcion, "no")==0) || (strcmp(opcion, "No")==0) || (strcmp(opcion, "n")==0)){
					    pregunta=FALSE;
				    }
				    else{
					    printf("opcion invalida.\n");
					    printf("ingrese nuevamente\n");
					    opcion=leer_teclado();
				    }
				    
			    }
    			    
			    printf("ingrese rut del cliente al cual desea modificar sus datos(q salir)\n");
			    rut=leer_teclado();				
			    printf("\n");
			    if(strcmp(rut, "q")==0){
				    break;
			    }
			    
			    temp=buscar_cliente_rut(rut, lista_c);
			    
			    if(temp != NULL){					
				    modificarCliente(temp);
				    guardar_lista_clientes(lista_c);
			    }
			    else
				    printf("cliente no encontrado\n");
			    break;
		    case 8: // salir
			    menu=FALSE;
			    break;         
			
		    default:
			    printf("\nOpcion invalida\n");
			    printf("Intente nuevamente\n\n");
			    break;
	    }
    }
}

void menuVendedor(char *id_vendedor){
	int opc,opc2;
	int numBoleta=0;
	int menu=TRUE;
	char *nombre=NULL;
	char *apellido=NULL;
	char *rut=NULL;
	char *calle=NULL;
	char *comuna=NULL;
	char *ciudad=NULL;
	char *tel_fijo=NULL;
	char *tel_movil=NULL;
	char *mail=NULL;
	struct Lista_ventas *temp=NULL;
	struct Lista_c *temp2=NULL;


	printf("\n\tMenu Vendedor.\n\n");

	while(menu){
		printf("Seleccione que desea hacer:\n\n");
		printf("1) Realizar una venta\n");
		printf("2) Ingresar nuevo cliente\n");
		printf("3) Modificar datos de un cliente\n");
		printf("4) Anular boleta\n");
		printf("5) Listar clientes\n");
		printf("6) Listar las compras de un cliente\n");
		printf("7) Salir del menu vendedor\n");
		scanf("%i", &opc);
		fflush(stdin);
	    	switch(opc)
    		{
			case 1:// nueva venta
			       hacerVenta(lista_c, lista_prod, lista_ventas, id_vendedor);
			       break;
			case 2:// ingresa cliente
				nombre=malloc(sizeof(char)*15);
				apellido=malloc(sizeof(char)*15);
				rut=malloc(sizeof(char)*15);
				calle=malloc(sizeof(char)*15);
				comuna=malloc(sizeof(char)*15);
				ciudad=malloc(sizeof(char)*15);
				tel_fijo=malloc(sizeof(char)*15);
				tel_movil=malloc(sizeof(char)*15);
				mail=malloc(sizeof(char)*15);	
			

   				printf("Ingrese datos del cliente:\n");
				printf("Nombre:\n");
				nombre=leer_teclado();
		    		printf("Apellido:\n");
				apellido=leer_teclado();
		    		printf("Rut:\n");
				rut=leer_teclado();
		    		printf("Ingrese datos de la direccion:\nCalle:\n");
				calle=leer_teclado();
				printf("Comuna:\n");
				comuna=leer_teclado();
				printf("Ciudad\n");
				ciudad=leer_teclado();
		    		printf("Telefono Fijo:\n");
				tel_fijo=leer_teclado();
		    		printf("Telefono Movil:\n");
				tel_movil=leer_teclado();
		    		printf("Correo electronico:\n");
				mail=leer_teclado();

				if(comprobar_rut(rut)==TRUE)
				{
					// compara si los ruts de la lista de cliente con el ingresado y devuelve 1 si se repiden los ruts de los clientes
					if(comparar_cliente_rut(rut, lista_c) != 1)
					{
		    				agregarCliente(nombre, apellido, rut, calle, comuna, ciudad, tel_fijo, tel_movil, mail,0 ,0 , lista_c);
						if(guardar_cliente(lista_c) == 1)
							printf("[!] archivo guardado con exito!\n");
						
						else
							printf("[!] error al guardar\n");
					}
					else
						printf("ya existe un cliente con el rut ingresado\n");
					
				}
				else
					printf("\nRut invalido\n");

				nombre=NULL;
				apellido=NULL;
				rut=NULL;
				calle=NULL;
				comuna=NULL;
				ciudad=NULL;
				tel_fijo=NULL;
				tel_movil=NULL;
				mail=NULL;
	       			break;
			case 3: // modifica datos clientes
				rut=malloc(sizeof(char)*15);
				printf("ingrese rut del cliente al cual desea modificar sus datos\n");
				rut=leer_teclado();				
				printf("\n");
				temp2=buscar_cliente_rut(rut, lista_c);

				if(temp2 != NULL){					
					modificarCliente(temp2);
					guardar_lista_clientes(lista_c);
				}
				else
					printf("Cliente no encontrado\n");

				nombre=NULL;
				apellido=NULL;
				temp2=NULL;
				break;
			case 4: // anular boleta
				printf("ingrese numero de la boleta\n");
				scanf("%i", &numBoleta);
				fflush(stdin);
				temp=buscar_venta(numBoleta, lista_ventas);
				if(temp!=NULL){
					eliminar_venta(lista_ventas);		
				}
				else{
					printf("no se encontro la boleta\n");
				}
				break;
			case 5: // listar clientes
				printf("con que datos?\n");
				printf("1) rut\n");
				printf("2) direccion\n");
				printf("3) telefono fijo\n");
				printf("4) telefono movil\n");
				printf("5) mail\n");
				printf("6) descuento\n");
				printf("7) cantidad de compras\n");
				printf("opcion: ");
				scanf("%i", &opc2);
				fflush(stdin);
				switch(opc2){
					case 1:
						listarClientes_rut(lista_c);
						break;
					case 2:
						listarClientes_direccion(lista_c);
						break;
					case 3:
						listarClientes_tel_fijo(lista_c);
						break;
					case 4:
						listarClientes_tel_movil(lista_c);
						break;
					case 5:
						listarClientes_mail(lista_c);
						break;
					case 6:
						listarClientes_descuento(lista_c);
						break;
					case 7:
						listarClientes_cantCompras(lista_c);
						break;	
					default:
						printf("Opcion invalida\n");
						break;
				}
				break;
			case 6: // listar las compras de un cliente
				rut=nombre=malloc(sizeof(char)*15);
				printf("Ingrese rut del cliente.\n");
				rut=leer_teclado();
				if(lista_ventas->sgte!=NULL){
					mostrarComprasClientes(rut, lista_ventas);	
				}
				else
					printf("\nNo hay compras registradas.\n");
				break;
			case 7: // salir
				menu=FALSE;
				break;
			default:
				printf("\nOpcion invalida\n");
				printf("Intente nuevamente\n\n");
				break;
		}
	}
}

int menuPrincipal(){

    int opc;
    int menu=TRUE;
    char *usuario = "admin";
    char *password = "eda2012";
    char *usuarioIngresado=malloc(sizeof(char)*10);
    char *passIngresada=malloc(sizeof(char)*10);
    char *id = malloc(sizeof(char)*10);

    while(menu){
	    opc=0;
	    printf("\t\nMenu principal\n\n");
	    printf("Seleccione modo de ingreso:\n");
       	    printf("1) Administrador\n");
	    printf("2) Vendedor\n");
	    printf("3) salir\nOpcion: ");
	    scanf("%i", &opc);
	    fflush(stdin);
	    switch(opc){
		    case 1:
	 		     printf("Ingrese usuario('q' salir):\n");
			     usuarioIngresado=leer_teclado();
			     if(strcmp(usuarioIngresado, "q")!=0)
			     {
				     printf("Ingrese password('q' salir):\n");
				     passIngresada=leer_teclado();
				     if(strcmp(passIngresada, "q")!=0){
					      if((strcmp(usuarioIngresado, usuario)==0) && (strcmp(passIngresada, password)==0))
				      	      {	   
	 					      menuAdmin();
				     	      }
					      else{
						      printf("Usuario o password invalidas\n");
					 	      printf("Intente nuevamente\n");						
					      }					
				     }				     
	 			     else{
					     break;
	 				     
	 			     }
	 		     }
	 		     else{
				     break;
			     }   	 		     
	 		     break;
     		     case 2: // modo vendedor
 				printf("Ingrese ID: \n");
				id=leer_teclado();
				if(buscar_id(id, lista_v) == TRUE)
					menuVendedor(id);
				
				else
					printf("ID no registrado\n");
				
				break;

     		     case 3: // salir
	 		     menu=FALSE;
	 		     return 1;   

     		     default:
	 		     printf("\nOpcion invalida\n");
	 		     printf("Intente nuevamente\n\n");
	 		     break;
     	     }
    }
    return 0;
}



int main()
{
	lista_v = crear_v();
	lista_c = crear_c();
	final = crear_v();
	lista_ventas=crear_ventas();
	boleta_d = crear();
	lista_prod = crear_prod();

	if(cargar_lista_vendedores(lista_v,final) == -1)
		printf("[!] Error al cargar la lista de vendedores\n    No cargada.\n");		
	else
		printf("[!] Lista de vendedores cargada con exito\n");
	
	if(cargarListaClientes(lista_c) == -1)
		printf("[!] Error al cargar la lista de clientes\n    No cargada.\n");		
	else
		printf("[!] Lista de clientes cargada con exito\n");
    	
	if(cargar_prod(lista_prod) == -1)
		printf("[!] Error al cargar la lista de productos\n    No cargada.\n");
	else
		printf("[!] Lista de productos cargada con exito\n");

	menuPrincipal();
	
	//cargar_lista_boletas(lista_ventas);
		

	return 0;
}
