#ifndef _H_LISTAPROD_
#define _H_LISTAPROD_


struct Lista_prod
{
	char *id;
	char *producto;
	int precio;
	int stock;
	struct Lista_prod *sgte;
};

struct Lista_prod *crear_prod();

int cargar_prod(struct Lista_prod *p);

void agregar_prod(char *id, char *producto, int precio, int stock, struct Lista_prod *p);
void mostrar_productos(struct Lista_prod *p);

struct Lista_prod *buscar_id_prod(char *id, struct Lista_prod *p);

int restar_stock(int cant, struct Lista_prod *lista);

#endif
