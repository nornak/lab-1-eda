#ifndef _H_LISTAVENDEDORES_
#define _H_LISTAVENDEDORES_

struct Lista_v{
	char *nombre;
	char *apellido;
	char *rut;
	char *direccion;
	char *tel_fijo;
	char *tel_movil;
	char *mail;
	char *tipo_contrato;
	char *id;
	struct Lista_v *sgte;
};

struct Lista_v *crear_v();

char *sacar_caracter(char caracter, char *cadena);

char *conseguir_id(char *rut);

void agregarVendedor(char *nombre,char *apellido, char *rut, char *direccion, char *tel_fijo, char *tel_movil, char *mail, char *tipo_contrato, struct Lista_v *p);

void agregarVendedor2(char *nombre,char *apellido, char *rut, char *direccion, char *tel_fijo, char *tel_movil, char *mail, char *tipo_contrato, struct Lista_v *p, struct Lista_v *final);

void eliminarVendedor(struct Lista_v *p);

int despedirVendedor(char *id, struct Lista_v *p);

void mostrarVendedores(struct Lista_v *p);

int guardar_lista_vendedores_ALL(struct Lista_v *p);

int guardar_vendedor(struct Lista_v *p);

int buscar_id(char *id,struct Lista_v *p);

int cargar_lista_vendedores(struct Lista_v *lista,struct Lista_v *final);

int comparar_ruts(char *rut, struct Lista_v *lista_v);

unsigned int tamanio_archivo(FILE *archivo);

char *leer_teclado();

#endif
