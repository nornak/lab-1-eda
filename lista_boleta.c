#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if !defined(__APPLE__) 
#include <malloc.h>
#endif
#include "lista_boleta.h"



struct boletaD *crear()
{
	struct boletaD *l=(struct boletaD *)malloc(sizeof(struct boletaD));
	l->sgte=NULL;
	l->id=NULL;
	l->cantProd=0;
	l->nombreProd=NULL;
	l->precio=0;
	return l;
};

void agregarBoletaD(char *id, char *nombreProd, int cantProd, int precio ,struct boletaD *boleta)
{
	struct boletaD *temp=(struct boletaD *)malloc(sizeof(struct boletaD));
	temp->sgte=boleta->sgte;

	temp->id=id;
	temp->cantProd=cantProd;
	temp->nombreProd=nombreProd;	
	temp->precio=precio;
	boleta->sgte=temp;
}

void eliminarBoletaD(struct boletaD *p)
{
	struct boletaD *q;
	q=p->sgte;
	p->sgte=p->sgte->sgte;
	free(q);
}

void eliminarLista(struct boletaD *h)
{
	struct boletaD *temp=h;

	while(temp->sgte != NULL)
	{
		free(temp->id);
		//temp->id=NULL;
		free(temp->nombreProd);
		//temp->nombreProd=NULL;
		free(temp);
		//temp=NULL;
		temp = temp->sgte;
	}
}


void mostrar_boletas(struct boletaD *h)
{
	struct boletaD *temp=h;
	while(temp->sgte!=NULL)
	{
		printf("id: %s producto: %s cantidad: %i precio:%i\n", temp->sgte->id, temp->sgte->nombreProd,temp->sgte->cantProd ,temp->sgte->precio);
		temp=temp->sgte;
	}

}

size_t largo_boleta(struct boletaD *lista_boleta)
{
	struct boletaD *temp=lista_boleta;
	size_t largo=0;

	while(temp->sgte != NULL){
		largo+=strlen(temp->sgte->id);
		largo+=strlen(temp->sgte->nombreProd);
		largo+=sizeof(temp->sgte->precio);
		largo+=sizeof(temp->sgte->cantProd);
		temp=temp->sgte;
	}
	return largo;
}



char *listaBoleta_a_char(struct boletaD *lista_boleta){
	struct boletaD *temp=lista_boleta;
	size_t largo=largo_boleta(lista_boleta);	
	char *string=malloc(sizeof(char)*largo);
	char *string2=malloc(sizeof(char)*largo);
	memset(string, '\0', largo+1);
	memset(string2, '\0', largo+1);
	if(string!=NULL){
		while(temp->sgte!=NULL)
		{
			if(temp->sgte->sgte==NULL)
			{
				//id, cantidad, nombre del producto y precio total
				sprintf(string2, "%s %i %s %i",temp->sgte->id,temp->sgte->cantProd,temp->sgte->nombreProd,temp->sgte->precio);
				strcat(string, string2);
			}
			else{
				sprintf(string2, "%s %i %s %i\n",temp->sgte->id,temp->sgte->cantProd,temp->sgte->nombreProd,temp->sgte->precio);
				strcat(string, string2);
			}
			temp=temp->sgte;
		}
	}
	return string;
}
