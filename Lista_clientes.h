#ifndef _H_LISTACLIENTES_
#define _H_LISTACLIENTES_


struct Lista_c
{
    char *nombre;
    char *apellido;
    char *rut;
    char *calle;
    char *comuna;
    char *ciudad;
    char *tel_fijo;
    char *tel_movil;
    char *mail;
    int descuento;
    int cantCompras;
    struct Lista_c *sgte;
};

struct Lista_c *crear_c();

void agregarCliente(char *nombre, char *apellido, char *rut, char *calle, char *comuna, char *ciudad, char *tel_fijo, char *tel_movil, char *mail,int descuento,int cantCompras, struct Lista_c *p);

void listarClientes_rut(struct Lista_c *p);

void listarClientes_direccion(struct Lista_c *p);

void listarClientes_tel_fijo(struct Lista_c *p);

void listarClientes_tel_movil(struct Lista_c *p);

void listarClientes_mail(struct Lista_c *p);

void listarClientes_descuento(struct Lista_c *p);

void listarClientes_cantCompras(struct Lista_c *p);

void modificarCliente(struct Lista_c *p);

struct Lista_c *buscar_cliente(char *nombre, char *apellido, struct Lista_c *p);

int guardar_lista_clientes(struct Lista_c *p);

int cargarListaClientes(struct Lista_c *lista);

int guardar_cliente(struct Lista_c *p);

int comparar_cliente_rut(char *rut, struct Lista_c *p);

struct Lista_c *buscar_cliente(char *nombre, char *apellido, struct Lista_c *p);

struct Lista_c *buscar_cliente_rut(char *rut, struct Lista_c *p);
#endif
